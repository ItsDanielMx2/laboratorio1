package labs.labfour;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * UniArrayList.
 */

public class UniArrayList<T> implements List<T>, Sortable<T>, Unique<T> {
  private Object[] array;
  private int size;

  public UniArrayList() {
    this.array = new Object[0];
    this.size = 0;
  }

  /**
   * Construye un nuevo UniArrayList a partir de un array de elementos.
   *
   * @param elements Array de elementos que se agregarán al UniArrayList.
   * @throws NullPointerException Si el array de elementos es nulo.
   */
  public UniArrayList(T[] elements) {
    this.array = new Object[elements.length];
    for (int i = 0; i < elements.length; i++) {
      this.array[i] = elements[i];
    }
    this.size = elements.length;
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public boolean contains(Object o) {
    return false;
  }

  @Override
  public Iterator<T> iterator() {
    return new Iterator<T>() {
      private int currentIndex = 0;

      @Override
      public boolean hasNext() {
        return currentIndex < size;
      }

      @Override
      public T next() {
        if (!hasNext()) {
          throw new NoSuchElementException();
        }
        return (T) array[currentIndex++];
      }
    };
  }

  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public <T1> T1[] toArray(T1[] a) {
    return null;
  }

  @Override
  public boolean add(T element) {
    ensureCapacity(size + 1);
    array[size] = element;
    size++;
    return true;
  }

  @Override
  public void add(int index, T element) {
    if (index < 0 || index > size) {
      throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
    }

    ensureCapacity(size + 1);

    // Mover los elementos a la derecha desde la posición index hasta el final
    for (int i = size; i > index; i--) {
      array[i] = array[i - 1];
    }

    array[index] = element;
    size++;
  }

  @Override
  public String toString() {
    StringBuilder result = new StringBuilder("[");
    for (int i = 0; i < size; i++) {
      result.append(array[i]);
      if (i < size - 1) {
        result.append(", ");
      }
    }
    result.append("]");
    return result.toString();
  }

  @Override
  public boolean remove(Object o) {
    for (int i = 0; i < size; i++) {
      if (Objects.equals(o, array[i])) {
        for (int j = i; j < size - 1; j++) {
          array[j] = array[j + 1];
        }
        array[size - 1] = null;
        size--;
        return true;
      }
    }
    return false;
  }

  @Override
  public T remove(int index) {
    if (index < 0 || index >= size) {
      throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
    }

    final T removedElement = (T) array[index];

    for (int i = index; i < size - 1; i++) {
      array[i] = array[i + 1];
    }

    array[size - 1] = null;
    size--;

    return removedElement;
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    return false;
  }

  @Override
  public boolean addAll(Collection<? extends T> c) {
    return false;
  }

  @Override
  public boolean addAll(int index, Collection<? extends T> c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    return false;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    return false;
  }

  @Override
  public void clear() {

  }

  @Override
  public T get(int index) {
    if (index < 0 || index >= size) {
      throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
    }
    return (T) array[index];
  }

  @Override
  public T set(int index, T element) {
    return null;
  }

  @Override
  public int indexOf(Object o) {
    return 0;
  }

  @Override
  public int lastIndexOf(Object o) {
    return 0;
  }

  @Override
  public ListIterator<T> listIterator() {
    return null;
  }

  @Override
  public ListIterator<T> listIterator(int index) {
    return null;
  }

  @Override
  public List<T> subList(int fromIndex, int toIndex) {
    return null;
  }

  @Override
  public void sort() {
    for (int i = 0; i < size - 1; i++) {
      for (int j = 0; j < size - i - 1; j++) {
        Comparable<T> current = (Comparable<T>) array[j];
        Comparable<T> next = (Comparable<T>) array[j + 1];
        if (current.compareTo((T) next) > 0) {
          // Swap elements
          Object temp = array[j];
          array[j] = array[j + 1];
          array[j + 1] = temp;
        }
      }
    }
  }

  @Override
  public void sortBy(Comparator<T> comparator) {
    for (int i = 0; i < size - 1; i++) {
      for (int j = 0; j < size - i - 1; j++) {
        T current = (T) array[j];
        T next = (T) array[j + 1];
        if (comparator.compare(current, next) > 0) {
          // Swap elements
          Object temp = array[j];
          array[j] = array[j + 1];
          array[j + 1] = temp;
        }
      }
    }
  }

  @Override
  public void unique() {
    UniArrayList<T> uniqueList = new UniArrayList<>();

    for (int i = 0; i < size; i++) {
      T current = (T) array[i];
      boolean isUnique = true;

      for (int j = 0; j < uniqueList.size(); j++) {
        T existing = uniqueList.get(j);
        if (current.equals(existing)) {
          isUnique = false;
          break;
        }
      }

      if (isUnique) {
        uniqueList.add(current);
      }
    }

    array = uniqueList.array;
    size = uniqueList.size;
  }

  private void ensureCapacity(int minCapacity) {
    if (minCapacity > array.length) {
      int newCapacity = Math.max(array.length * 2, minCapacity);
      Object[] newArray = new Object[newCapacity];
      for (int i = 0; i < size; i++) {
        newArray[i] = array[i];
      }
      array = newArray;
    }
  }
}