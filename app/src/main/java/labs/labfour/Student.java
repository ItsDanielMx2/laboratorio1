package labs.labfour;

import java.util.Comparator;
import java.util.Objects;

/**
 * Student.
 */
public class Student implements Comparable<Student> {
  private String name;
  private int grade;

  public Student(String name, int grade) {
    this.name = name;
    this.grade = grade;
  }

  public int getGrade() {
    return grade;
  }

  @Override
  public int compareTo(Student other) {
    return this.name.compareTo(other.name);
  }

  public static Comparator<Student> byGrade() {
    return Comparator.comparingInt(Student::getGrade);
  }

  @Override
  public String toString() {
    return "{" + name + "," + grade + "}";
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    Student student = (Student) obj;
    return grade == student.grade && Objects.equals(name, student.name);
  }
}