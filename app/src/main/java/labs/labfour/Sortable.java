package labs.labfour;

import java.util.Comparator;

/**
 * Sortable.
 */
public interface Sortable<T> {
  void sort();

  void sortBy(Comparator<T> comparator);
}
