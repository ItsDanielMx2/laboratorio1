package labs.labfour;

/**
 * Unique.
 */
public interface Unique<T> {
  void unique();
}
