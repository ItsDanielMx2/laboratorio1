package labs.labtwo;

import java.util.List;

/**
 * Clase para realizar operaciones de exponente.
 */
public class Potencia implements Operacion {
  /**
   * Calcula la potencia de un número elevado a otra potencia.
   *
   * @param valores Lista de dos valores donde el primer elemento es la base
   *                y el segundo es el exponente.
   * @return Resultado de la potencia.
   * @throws IllegalArgumentException Si la lista de valores es nula
   *                o no tiene exactamente dos elementos.
   */
  public double realizar(List<Double> valores) {
    if (valores == null || valores.size() != 2) {
      throw new IllegalArgumentException(
        "Se requieren exactamente dos valores para calcular la potencia");
    }

    double base = valores.get(0);
    double exponente = valores.get(1);

    return Math.pow(base, exponente);
  }
}
