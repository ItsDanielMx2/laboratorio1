package labs.labtwo;

import java.util.List;

/**
 * Interfaz que define la operación a realizar sobre una lista de valores.
 * Implementaciones de esta interfaz deben proporcionar la lógica para
 * realizar la operación deseada.
 */
public interface Operacion {
  double realizar(List<Double> valores);
}