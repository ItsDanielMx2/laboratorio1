package labs.labtwo;

import java.util.List;

/**
 * Clase para realizar operaciones de división.
 */
public class Division implements Operacion {
  /**
   * Realiza una división entre dos números.
   *
   * @param valores Lista de valores donde el primer elemento es el dividendo
   *                y el segundo es el divisor.
   * @return Resultado de la división.
   * @throws ArithmeticException Si el divisor es cero.
   * @throws IllegalArgumentException Si la lista de valores es nula o tiene menos de dos elementos.
   */
  public double realizar(List<Double> valores) {
    if (valores == null || valores.size() < 2) {
      throw new IllegalArgumentException("Se requieren al menos dos valores para la división");
    }

    double a = valores.get(0);
    double b = valores.get(1);

    if (b != 0) {
      return a / b;
    } else {
      throw new ArithmeticException("No se puede dividir por cero");
    }
  }
}
