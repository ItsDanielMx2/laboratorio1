package labs.labtwo;

import java.util.Arrays;
import java.util.List;

/**
 * Clase para calcular el promedio de una lista de valores.
 */
public class Promedio {

  /**
   * Calcula el promedio de una lista de valores.
   *
   * @param valores Lista de valores para calcular el promedio.
   * @return Promedio de la lista de valores.
   */
  public double calcularPromedio(List<Double> valores) {
    if (valores == null || valores.isEmpty()) {
      throw new IllegalArgumentException("La lista de valores no puede estar vacia");
    }

    double sumaTotal = OperacionesBasicas.realizarOperacion(TipoOperacion.SUMA, valores);
    int cantidadValores = valores.size();

    List<Double> divisionValores = Arrays.asList(sumaTotal, (double) cantidadValores);
    double resultado = OperacionesBasicas.realizarOperacion(
        TipoOperacion.DIVISION, divisionValores);

    return resultado;
  }
}