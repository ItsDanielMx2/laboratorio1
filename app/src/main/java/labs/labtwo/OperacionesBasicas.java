package labs.labtwo;

import java.util.List;

/**
 * Clase que proporciona métodos para realizar operaciones básicas utilizando objetos
 * que implementan la interfaz {@link Operacion}.
 */

public class OperacionesBasicas {

  /**
   * Realiza la operación especificada sobre la lista de valores dada.
   *
   * @param tipoOperacion Tipo de operación a realizar (suma, resta, multiplicación, etc.).
   * @param valores Lista de valores sobre la cual realizar la operación.
   * @return Resultado de la operación.
   * @throws IllegalArgumentException Si el tipo de operación no es válido
   *                o la lista de valores es nula.
   */
  public static double realizarOperacion(TipoOperacion tipoOperacion, List<Double> valores) {
    Operacion operacion = obtenerOperacion(tipoOperacion);
    return operacion.realizar(valores);
  }

  /**
   * Obtiene la implementación de la interfaz {@link Operacion}
   * correspondiente al tipo de operación dado.
   *
   * @param tipoOperacion Tipo de operación para el cual obtener la implementación.
   * @return Implementación de la interfaz {@link Operacion} para el tipo de operación dado.
   * @throws IllegalArgumentException Si el tipo de operación no es válido.
   */
  static Operacion obtenerOperacion(TipoOperacion tipoOperacion) {
    return switch (tipoOperacion) {
      case SUMA -> new Suma();
      case RESTA -> new Resta();
      case MULTIPLICACION -> new Multiplicacion();
      case DIVISION -> new Division();
      case POTENCIA -> new Potencia();
      case RAIZ_CUADRADA -> new Raiz();
      default -> throw new IllegalArgumentException("Tipo de operación no válido");
    };
  }
}