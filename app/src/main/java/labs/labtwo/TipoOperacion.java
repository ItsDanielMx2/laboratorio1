package labs.labtwo;

/**
 * Enumeración que representa los diferentes tipos de operaciones disponibles.
 * Cada valor de la enumeración corresponde a un tipo específico de operación matemática.
 */
public enum TipoOperacion {
  SUMA, RESTA, MULTIPLICACION, DIVISION, POTENCIA, RAIZ_CUADRADA
}
