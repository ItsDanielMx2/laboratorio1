package labs.labtwo;

import labs.labtwo.Figuras;

/**
 * Clase que representa un rectángulo.
 */
class Rectangulo extends Figuras {
  private double base;
  private double altura;

  /**
   * Constructor para crear un rectángulo.
   *
   * @param base   La longitud de la base del rectángulo.
   * @param altura La altura del rectángulo.
   */
  public Rectangulo(double base, double altura) {
    super(4);
    this.base = base;
    this.altura = altura;
  }

  /**
   * Obtiene la longitud de la base del rectángulo.
   *
   * @return La longitud de la base del rectángulo.
   */
  public double getBase() {
    return base;
  }

  /**
   * Obtiene la altura del rectángulo.
   *
   * @return La altura del rectángulo.
   */
  public double getAltura() {
    return altura;
  }

  @Override
  public double perimetro() {
    return 2 * (base + altura);
  }

  @Override
  public double area() {
    return base * altura;
  }
}
