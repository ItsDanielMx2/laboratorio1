package labs.labtwo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Clase para el Software Matemático Avanzado.
 */

public class LabTwo {
  private Map<String, Double> variables;

  public LabTwo() {
    this.variables = new HashMap<>();
  }

  public void asignarVariable(String variable, double valor) {
    variables.put(variable, valor);
  }

  public double calcularOperacion(TipoOperacion tipoOperacion, List<Double> valores) {
    Operacion operacion = OperacionesBasicas.obtenerOperacion(tipoOperacion);
    return operacion.realizar(valores);
  }

  public double calcularPromedio(List<Double> valores) {
    Promedio promedio = new Promedio();
    return promedio.calcularPromedio(valores);
  }

  public double resolverExpresion(String expresion) {
    ExpresionesAlgebraicas expresionesAlgebraicas = new ExpresionesAlgebraicas(variables);
    return expresionesAlgebraicas.resolverExpresion(expresion);
  }

  public double resolverEcuacionDiferencial(String ecuacion) {
    EcuacionesDif ecuacionesDiferenciales = new EcuacionesDif();
    return ecuacionesDiferenciales.resolverEcuacionDiferencial(ecuacion);
  }

  public double resolverIntegral(String funcion, double a, double b) {
    Integrales integrales = new Integrales();
    return integrales.resolverIntegral(funcion, a, b);
  }

  public double calcularAreaCuadrado(double lado) {
    Cuadrado cuadrado = new Cuadrado(lado);
    return cuadrado.area();
  }

  public double calcularPerimetroCuadrado(double lado) {
    Cuadrado cuadrado = new Cuadrado(lado);
    return cuadrado.perimetro();
  }

  public double calcularAreaTriangulo(double a, double b, double c) {
    Triangulo triangulo = new Triangulo(a, b, c);
    return triangulo.area();
  }

  public double calcularPerimetroTriangulo(double a, double b, double c) {
    Triangulo triangulo = new Triangulo(a, b, c);
    return triangulo.perimetro();
  }

  public double calcularAreaRectangulo(double base, double altura) {
    Rectangulo rectangulo = new Rectangulo(base, altura);
    return rectangulo.area();
  }

  public double calcularPerimetroRectangulo(double base, double altura) {
    Rectangulo rectangulo = new Rectangulo(base, altura);
    return rectangulo.perimetro();
  }

  public double calcularAreaCirculo(double radio) {
    Circulo circulo = new Circulo(radio);
    return circulo.area();
  }

  public double calcularPerimetroCirculo(double radio) {
    Circulo circulo = new Circulo(radio);
    return circulo.perimetro();
  }

}