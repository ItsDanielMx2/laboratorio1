package labs.labtwo;

import java.util.Map;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

/**
 * Clase para expresiones algebraicas.
 */
public class ExpresionesAlgebraicas {

  private Map<String, Double> variables;

  public ExpresionesAlgebraicas(Map<String, Double> variables) {
    this.variables = variables;
  }

  /**
   * Resuelve una expresión algebraica con variables asignadas previamente.
   *
   * @param expresion La expresión algebraica a resolver.
   * @return El resultado de evaluar la expresión.
   */

  public double resolverExpresion(String expresion) {
    Expression e = new ExpressionBuilder(expresion)
            .variables(variables.keySet())
            .build();
    variables.forEach(e::setVariable);
    return e.evaluate();
  }
}