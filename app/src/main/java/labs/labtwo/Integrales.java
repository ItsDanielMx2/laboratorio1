package labs.labtwo;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import org.apache.commons.math3.analysis.integration.TrapezoidIntegrator;

/**
 * Clase para integrales.
 */

public class Integrales {
  /**
   * Resuelve la integral definida de la función dada en el intervalo [a, b] utilizando
   * el método del trapezoide.
   *
   * @param funcion La función a integrar.
   * @param a       El límite inferior del intervalo.
   * @param b       El límite superior del intervalo.
   * @return El resultado de la integral definida.
   */
  public double resolverIntegral(String funcion, double a, double b) {
    Expression e = new ExpressionBuilder(funcion)
            .variables("x")
            .build();

    TrapezoidIntegrator integrator = new TrapezoidIntegrator();
    double resultado = integrator.integrate(1000, x -> {
      e.setVariable("x", x);
      return e.evaluate();
    }, a, b); // desde x=a hasta x=b

    return resultado;
  }
}
