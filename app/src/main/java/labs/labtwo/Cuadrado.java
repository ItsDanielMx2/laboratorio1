package labs.labtwo;

import labs.labtwo.Figuras;

/**
 * Clase que representa un cuadrado.
 */
class Cuadrado extends Figuras {
  private double lado;

  /**
   * Constructor para crear un cuadrado.
   *
   * @param lado La longitud de un lado del cuadrado.
   */
  public Cuadrado(double lado) {
    super(4);
    this.lado = lado;
  }

  /**
   * Obtiene la longitud de un lado del cuadrado.
   *
   * @return La longitud de un lado del cuadrado.
   */
  public double getLado() {
    return lado;
  }

  @Override
  public double perimetro() {
    return 4 * lado;
  }

  @Override
  public double area() {
    return Math.pow(lado, 2);
  }
}