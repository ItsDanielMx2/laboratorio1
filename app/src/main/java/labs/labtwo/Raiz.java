package labs.labtwo;

import java.util.List;

/**
 * Clase para realizar operaciones de raíz cuadrada.
 */

public class Raiz implements Operacion {
  /**
   * Calcula la raíz cuadrada de un número.
   *
   * @param valores Lista de un solo valor, el número del cual se desea calcular la raíz cuadrada.
   * @return Resultado de la raíz cuadrada.
   * @throws IllegalArgumentException Si la lista de valores es nula
   *         o no tiene exactamente un elemento.
   * @throws ArithmeticException Si el número es negativo.
   */
  public double realizar(List<Double> valores) {
    if (valores == null || valores.size() != 1) {
      throw new IllegalArgumentException(
        "Se requiere exactamente un valor para calcular la raíz cuadrada");
    }

    double numero = valores.get(0);

    if (numero < 0) {
      throw new ArithmeticException("No se puede calcular la raíz cuadrada de un número negativo");
    }

    return Math.sqrt(numero);
  }
}
