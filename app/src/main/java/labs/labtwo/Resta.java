package labs.labtwo;

import java.util.List;

/**
 * Clase para realizar operaciones de resta.
 */
public class Resta implements Operacion {
  /**
   * Realiza una resta de los valores proporcionados.
   *
   * @param valores Lista de valores a restar.
   * @return Resultado de la resta.
   */
  public double realizar(List<Double> valores) {
    if (valores == null || valores.size() < 2) {
      throw new IllegalArgumentException("Se requieren al menos dos valores para la resta");
    }

    double resultado = valores.get(0);
    for (int i = 1; i < valores.size(); i++) {
      resultado -= valores.get(i);
    }

    return resultado;
  }
}
