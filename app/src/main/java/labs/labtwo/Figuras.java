package labs.labtwo;

/**
 * Clase abstracta que sirve como base para otras figuras geométricas.
 */
abstract class Figuras {
  private int numeroLados;

  /**
   * Constructor para crear una figura geométrica.
   *
   * @param numeroLados Número de lados de la figura.
   */
  public Figuras(int numeroLados) {
    this.numeroLados = numeroLados;
  }

  /**
   * Obtiene el número de lados de la figura.
   *
   * @return El número de lados de la figura.
   */
  public int getNumeroLados() {
    return numeroLados;
  }

  /**
   * Calcula el perímetro de la figura.
   *
   * @return El perímetro de la figura.
   */
  public abstract double perimetro();

  /**
   * Calcula el área de la figura.
   *
   * @return El área de la figura.
   */
  public abstract double area();
}