package labs.labtwo;

import labs.labtwo.Figuras;

/**
 * Clase que representa un triángulo.
 */
class Triangulo extends Figuras {
  private double lado1;
  private double lado2;
  private double lado3;

  /**
   * Constructor para crear un triángulo.
   *
   * @param lado1 La longitud del primer lado del triángulo.
   * @param lado2 La longitud del segundo lado del triángulo.
   * @param lado3 La longitud del tercer lado del triángulo.
   */
  public Triangulo(double lado1, double lado2, double lado3) {
    super(3);
    this.lado1 = lado1;
    this.lado2 = lado2;
    this.lado3 = lado3;
  }

  /**
   * Obtiene la longitud del primer lado del triángulo.
   *
   * @return La longitud del primer lado del triángulo.
   */
  public double getLado1() {
    return lado1;
  }

  /**
   * Obtiene la longitud del segundo lado del triángulo.
   *
   * @return La longitud del segundo lado del triángulo.
   */
  public double getLado2() {
    return lado2;
  }

  /**
   * Obtiene la longitud del tercer lado del triángulo.
   *
   * @return La longitud del tercer lado del triángulo.
   */
  public double getLado3() {
    return lado3;
  }

  @Override
  public double perimetro() {
    return lado1 + lado2 + lado3;
  }

  @Override
  public double area() {
    double s = (lado1 + lado2 + lado3) / 2;
    return Math.sqrt(s * (s - lado1) * (s - lado2) * (s - lado3));
  }
}