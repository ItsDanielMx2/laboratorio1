package labs.labtwo;

import org.apache.commons.math3.ode.FirstOrderDifferentialEquations;
import org.apache.commons.math3.ode.FirstOrderIntegrator;
import org.apache.commons.math3.ode.nonstiff.ClassicalRungeKuttaIntegrator;

/**
 * Clase para ecuaciones diferenciales.
 */

public class EcuacionesDif {
  /**
   * Resuelve una ecuación diferencial ordinaria de primer orden.
   *
   * @param ecuacion La ecuación diferencial en el formato "dy/dt = f(y, t)".
   * @return El valor calculado después de integrar la ecuación.
   */

  public double resolverEcuacionDiferencial(String ecuacion) {
    FirstOrderDifferentialEquations ode = new FirstOrderDifferentialEquations() {
      public int getDimension() {
        return 1;
      }

      public void computeDerivatives(double t, double[] y, double[] ydot) {
        ydot[0] = y[0];
      }
    };

    FirstOrderIntegrator integrator = new ClassicalRungeKuttaIntegrator(1.0);
    double[] y = new double[] { 1.0 }; // valor inicial
    integrator.integrate(ode, 0.0, y, 1.0, y); // desde t=0 hasta t=1
    return y[0];
  }
}
