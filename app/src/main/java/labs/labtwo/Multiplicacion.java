package labs.labtwo;

import java.util.List;

/**
 * Clase para realizar operaciones de multiplicación.
 */
public class Multiplicacion implements Operacion {
  /**
   * Realiza una multiplicación de los valores proporcionados.
   *
   * @param valores Lista de valores a multiplicar.
   * @return Resultado de la multiplicación.
   */
  public double realizar(List<Double> valores) {
    if (valores == null || valores.isEmpty()) {
      throw new IllegalArgumentException("La lista de valores no puede estar vacía");
    }

    double resultado = 1;
    for (double valor : valores) {
      resultado *= valor;
    }

    return resultado;
  }
}
