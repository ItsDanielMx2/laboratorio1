package labs.labtwo;

import java.util.List;

/**
 * Clase para realizar operaciones de suma.
 */
public class Suma implements Operacion {
  /**
   * Realiza una suma de los valores proporcionados.
   *
   * @param valores Lista de valores a sumar.
   * @return Resultado de la suma.
   */
  public double realizar(List<Double> valores) {
    if (valores == null || valores.isEmpty()) {
      throw new IllegalArgumentException("La lista de valores no puede estar vacia");
    }

    double resultado = 0;
    for (double valor : valores) {
      resultado += valor;
    }

    return resultado;
  }
}