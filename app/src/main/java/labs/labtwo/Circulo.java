package labs.labtwo;

import labs.labtwo.Figuras;

/**
 * Clase que representa un círculo.
 */
class Circulo extends Figuras {
  private double radio;

  /**
   * Constructor para crear un círculo.
   *
   * @param radio El radio del círculo.
   */
  public Circulo(double radio) {
    super(0);
    this.radio = radio;
  }

  /**
   * Obtiene el radio del círculo.
   *
   * @return El radio del círculo.
   */
  public double getRadio() {
    return radio;
  }

  @Override
  public double perimetro() {
    return 2 * Math.PI * radio;
  }

  @Override
  public double area() {
    return Math.PI * Math.pow(radio, 2);
  }
}