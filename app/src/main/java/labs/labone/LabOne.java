package labs.labone;

/**
 * LabOne class.
 */
public class LabOne {

  /**
     * main method.
     *
     * @param args args.
     */
  public static void main(String[] args) {
    var res = process(args);
    System.out.printf(res);
  }

  static String process(String[] args) {
    var res = "";
    var o = new String[] {"op", "alg", "input", "-ct-p", "-ct-r"}; //aqui
    var v = new String[] {null, null, null, null, null}; // aca
    var idx = 0;

    if (args.length == 2 && args[0].startsWith("-") && args[1].startsWith("-")) {
      var key1 = args[0].substring(1, args[0].indexOf("="));
      var key2 = args[1].substring(1, args[1].indexOf("="));
      if ("input".equals(key1) && "op".equals(key2)
          && ("e".equals(args[1].substring(args[1].indexOf("=") + 1))
          || "d".equals(args[1].substring(args[1].indexOf("=") + 1)))) {
        res = args[0].substring(args[0].indexOf("=") + 1);
      } else if ("input".equals(key2) && "op".equals(key1)
          && ("e".equals(args[0].substring(args[0].indexOf("=") + 1))
          || "d".equals(args[0].substring(args[0].indexOf("=") + 1)))) {
        res = args[1].substring(args[1].indexOf("=") + 1);
      }
    } else if (args.length >= 3 && args[0].startsWith("-")
        && args[1].startsWith("-") && args[2].startsWith("-")) {
      for (int i = 0; i < o.length; i++) {
        for (int j = 0; j < args.length; j++) {
          var key = args[j].substring(1, args[j].indexOf("="));
          if (o[i].equals(key)) {
            v[i] = args[j].substring(args[j].indexOf("=") + 1);
            break;
          }
        }
      }

      String opv = null;
      for (int i = 0; i < o.length; i++) {
        if ("op".equals(o[i])) {
          opv = v[i];
          break;
        }
      }

      String algv = null;
      for (int i = 0; i < o.length; i++) {
        if ("alg".equals(o[i])) {
          algv = v[i];
          break;
        }
      }

      String input = null;
      for (int i = 0; i < o.length; i++) {
        if ("input".equals(o[i])) {
          input = v[i];
          break;
        }
      }

      String ctpv = null;
      for (int i = 0; i < o.length; i++) {
        if ("-ct-p".equals(o[i])) {
          ctpv = v[i];
          break;
        }
      }

      String ctrv = null;
      for (int i = 0; i < o.length; i++) {
        if ("-ct-r".equals(o[i])) {
          ctrv = v[i];
          break;
        }
      }

      // mas

      if ("none".equals(algv) && ("e".equals(opv) || "d".equals(opv))) {
        res = input;
      } else if ("alg1".equals(algv)) {
        String key = "ZYXWVUTSRQPONMLKJIHGFEDCBA";
        if ("e".equals(opv)) {
          StringBuilder sb = new StringBuilder();
          for (char ch : input.toCharArray()) {
            if (Character.isLetter(ch)) {
              char uch = Character.toUpperCase(ch);
              int index = uch - 'A';
              char ench = index >= 0 && index < key.length() ? key.charAt(index) : ch;
              sb.append(Character.isLowerCase(ch) ? Character.toLowerCase(ench) : ench);
            } else {
              sb.append(ch);
            }
          }

          res = sb.toString();

        } else if ("d".equals(opv)) {
          StringBuilder sb = new StringBuilder();
          for (char ch : input.toCharArray()) {
            if (Character.isLetter(ch)) {
              char uch = Character.toUpperCase(ch);
              int index = key.indexOf(uch);
              char dch = index >= 0 && index < key.length() ? (char) ('A' + index) : ch;
              sb.append(Character.isLowerCase(ch) ? Character.toLowerCase(dch) : dch);
            } else {
              sb.append(ch);
            }
          }

          res = sb.toString();
        }
      } else if ("alg2".equals(algv)) {
        if ("e".equals(opv) || "d".equals(opv)) {
          String hs = "ZYXWVUTSRQPONMLKJIHGFEDCBA";
          StringBuilder sb = new StringBuilder();
          for (char ch : input.toCharArray()) {
            for (char hsch : hs.toCharArray()) {
              ch = (char) (ch ^ hsch);
            }
            sb.append(ch);
          }
          res = sb.toString();
        }
      } else if ("ct".equals(algv)) {
        if ("e".equals(opv)) {
          if ("true".equals(ctrv) && ctpv == null) {
            StringBuilder inverted = new StringBuilder(input).reverse();
            res += inverted;
          } else if ("true".equals(ctrv) && ctpv != null) {
            StringBuilder inverted = new StringBuilder(input).reverse();
            res = ctpv + inverted + ctpv;
          } else {
            res = ctpv + input + ctpv;
          }
        } else if ("d".equals(opv)) {
          if ("true".equals(ctrv) && ctpv == null) {
            StringBuilder inverted = new StringBuilder(input).reverse();
            res += inverted;
          } else if (input.startsWith(ctpv) && input.endsWith(ctpv)) {
            if ("true".equals(ctrv)) {
              String withoutCtpv = input.substring(ctpv.length(), input.length() - ctpv.length());
              StringBuilder inverted = new StringBuilder(withoutCtpv).reverse();
              res += inverted;
            } else {
              res = input.substring(ctpv.length(), input.length() - ctpv.length());
            }
          } else {
            res = input;
          }
        }
      }
    }
    return res;
  }
}
