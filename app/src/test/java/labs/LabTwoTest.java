package labs;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import labs.labtwo.LabTwo;
import labs.labtwo.TipoOperacion;
import org.junit.jupiter.api.Test;

class LabTwoTest {

  @Test
  void resolverExpresionSimple() {
    LabTwo software = new LabTwo();
    software.asignarVariable("x", 5.0);
    double resultado = software.resolverExpresion("2 * x + 3");
    assertEquals(13.0, resultado, "La expresión debería dar 13.0");
  }

  @Test
  void resolverEcuacionDiferencial() {
    LabTwo software = new LabTwo();
    double resultado = software.resolverEcuacionDiferencial("dy/dt = y");
    assertEquals(2.718, resultado, 0.01, "La solución debería ser aproximadamente e=2.718");
  }

  @Test
  void resolverIntegral() {
    LabTwo software = new LabTwo();
    double resultado = software.resolverIntegral("2*x", 0.0, 1.0);
    assertEquals(1.0, resultado, 0.001, "La integral de 2x desde 0 hasta 1 debería ser 1.0");
  }

  @Test
  void sumarNumerosPositivos() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularOperacion(TipoOperacion.SUMA, Arrays.asList(3.0, 5.0, 2.0));
    assertEquals(10, resultado, 0.001);
  }

  @Test
  void sumarNumerosNegativos() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularOperacion(TipoOperacion.SUMA,
            Arrays.asList(-2.0, -4.0, -2.0));
    assertEquals(-8, resultado, 0.001);
  }

  @Test
  void restarNumerosPositivos() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularOperacion(TipoOperacion.RESTA,
            Arrays.asList(10.0, 2.0, 3.0));
    assertEquals(5.0, resultado, 0.001);
  }

  @Test
  void restarNumerosNegativos() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularOperacion(TipoOperacion.RESTA,
            Arrays.asList(-10.0, -2.0, -3.0));
    assertEquals(-5.0, resultado, 0.001);
  }

  @Test
  void multiplicarNumerosPositivos() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularOperacion(TipoOperacion.MULTIPLICACION,
            Arrays.asList(2.0, 3.0, 4.0));
    assertEquals(24.0, resultado, 0.001);
  }

  @Test
  void multiplicarNumerosNegativos() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularOperacion(TipoOperacion.MULTIPLICACION,
            Arrays.asList(-2.0, -3.0, -4.0));
    assertEquals(-24, resultado, 0.001);
  }

  @Test
  void dividirNumerosPositivos() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularOperacion(TipoOperacion.DIVISION,
            Arrays.asList(8.0, 2.0));
    assertEquals(4.0, resultado, 0.001);
  }

  @Test
  void dividirNumerosNegativos() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularOperacion(TipoOperacion.DIVISION,
            Arrays.asList(-10.0, -2.0));
    assertEquals(5.0, resultado, 0.001);
  }

  @Test
  void dividirPorCero() {
    LabTwo software = new LabTwo();
    assertThrows(ArithmeticException.class, () -> software.calcularOperacion(TipoOperacion.DIVISION,
            Arrays.asList(5.0, 0.0)));
  }

  @Test
  void elevarPotencia() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularOperacion(TipoOperacion.POTENCIA,
            Arrays.asList(2.0, 3.0));
    assertEquals(8.0, resultado, 0.001);
  }

  @Test
  void calcularRaizCuadrada() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularOperacion(TipoOperacion.RAIZ_CUADRADA,
            Arrays.asList(25.0));
    assertEquals(5.0, resultado, 0.001);
  }

  @Test
  void calcularPromedioListaVacia() {
    LabTwo software = new LabTwo();
    try {
      software.calcularPromedio(Arrays.asList());
    } catch (IllegalArgumentException e) {
      assert "La lista de valores no puede estar vacia".equals(e.getMessage());
    }
  }

  @Test
  void calcularPromedioValoresPositivos() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularPromedio(Arrays.asList(3.0, 5.0, 2.0));
    assertEquals(3.333, resultado, 0.001);
  }

  @Test
  void calcularPromedioValoresNegativos() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularPromedio(Arrays.asList(-2.0, -4.0, -6.0));
    assertEquals(-4.0, resultado, 0.001);
  }

  @Test
  void calcularAreaCuadrado() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularAreaCuadrado(5);
    assertEquals(25.0, resultado, 0.001, "El área del cuadrado debería ser 25.0");
  }

  @Test
  void calcularPerimetroCuadrado() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularPerimetroCuadrado(5);
    assertEquals(20.0, resultado, 0.001, "El perímetro del cuadrado debería ser 20.0");
  }

  @Test
  void calcularAreaCirculo() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularAreaCirculo(3);
    assertEquals(28.274, resultado, 0.001,
            "El área del círculo debería ser aproximadamente 28.274");
  }

  @Test
  void calcularPerimetroCirculo() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularPerimetroCirculo(3);
    assertEquals(18.849, resultado, 0.001,
            "El perímetro del círculo debería ser aproximadamente 18.849");
  }

  @Test
  void calcularAreaRectangulo() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularAreaRectangulo(6, 4);
    assertEquals(24.0, resultado, 0.001, "El área del rectángulo debería ser 24.0");
  }

  @Test
  void calcularPerimetroRectangulo() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularPerimetroRectangulo(6, 4);
    assertEquals(20.0, resultado, 0.001, "El perímetro del rectángulo debería ser 20.0");
  }

  @Test
  void calcularAreaTriangulo() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularAreaTriangulo(6, 5, 5);
    assertEquals(12.0, resultado, 0.001, "El área del triángulo debería ser 20.0");
  }

  @Test
  void calcularPerimetroTriangulo() {
    LabTwo software = new LabTwo();
    double resultado = software.calcularPerimetroTriangulo(6, 4, 5);
    assertEquals(15.0, resultado, 0.001,
            "El perímetro del triángulo debería ser aproximadamente 21.180");
  }
}
